package siample.dev;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@SpringBootApplication
// @Configuration
// @ComponentScan
// @EnableAutoConfiguration
public class Sb303BApplication {
	
	@Bean
	@ConfigurationProperties("app.db")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Autowired DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Sb303BApplication.class, args);
	}

}
