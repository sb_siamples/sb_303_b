package siample.dev.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.ApplicationScope;

@Repository
@ApplicationScope
public class InitDAO {
	
	@Autowired NamedParameterJdbcTemplate jdbc;
	
	private String[] users = {"a", "b", "c", "sb303_b",};

	public void dbStrukturaLetrehozas() {
		
		jdbc.getJdbcTemplate().execute("DROP TABLE IF exists login_user");
		jdbc.getJdbcTemplate().execute("CREATE TABLE login_user "
				+ "(id bigint primary key, username varchar(100), password varchar(100))");
		
		for (int i=0; i< users.length; i++) {
			
			jdbc.update("INSERT INTO login_user(id, username, password) VALUES (:id, :username, :password)", 
					new MapSqlParameterSource()
						.addValue("id", i+1)
						.addValue("username", users[i])
						.addValue("password", users[i]));
		}
	}
}
