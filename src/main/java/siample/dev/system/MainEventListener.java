package siample.dev.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import siample.dev.dao.InitDAO;

@Component
public class MainEventListener {

	@Autowired InitDAO initDAO;
	@Autowired ApplicationContext aContext;
	
	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
	// ContextRefreshedEvent raised when aContext initialized/refreshed
		
		initDAO.dbStrukturaLetrehozas();
		
	}
}
